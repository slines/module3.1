﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let me tell your fate.");
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (Int32.TryParse(source, out int number))
                return number;
            throw new ArgumentException();
        }

        public int Multiplication(int num1, int num2)
        {
            int res = 0;
            for (int i = 0; i < Math.Abs(num2); i++)
                res += Math.Abs(num1);
            if (num1 < 0)
                res = -res;
            if (num2 < 0)
                res = -res;
            return res;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (Int32.Parse(input) < 0)
            {
                Console.WriteLine("Введите натуральное число еще раз.");
                input = Console.ReadLine();
            }
            return Int32.TryParse(input, out result);
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            int count = 0;
            List<int> arr = new List<int>();
            if (count == naturalNumber)
                return arr;
            for (int i = 0; ; i += 2)
            {
                if (i % 2 == 0)
                {
                    arr.Add(i);
                    count++;
                }  
            }
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (Int32.Parse(input) < 0)
            {
                Console.WriteLine("Введите натуральное число еще раз.");
                input = Console.ReadLine();
            }
            return Int32.TryParse(input, out result);
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string number = source.ToString();
            return number.Remove(number.IndexOf(digitToRemove.ToString()), 1);
        }
    }
}
